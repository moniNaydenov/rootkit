#!/bin/bash
TARGETVMS="$1"
ssh-copy-id $TARGETVMS
scp rootkit.c $TARGETVMS:~/
scp Makefile $TARGETVMS:~/
scp run.sh $TARGETVMS:~/

#TARGETVMS="rootkit2"
#scp rootkit.c $TARGETVMS:~/rootkit/
#scp Makefile $TARGETVMS:~/rootkit/
#scp run.sh $TARGETVMS:~/rootkit/
#scp print_to_buff.sh $TARGETVMS:~/rootkit/

