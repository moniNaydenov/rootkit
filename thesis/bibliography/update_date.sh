#!/bin/bash
if [ -z "$1" ]; then
    echo "Usage: $0 '2019-12-20'"
else
    sed -i "s/urldate.*\$/urldate = \{$1\}/" references.bib
fi
