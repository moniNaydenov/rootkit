%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Problem definition and approach}
\label{sec:problemdefinitionn}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The following chapter provides a more in-depth understanding of how passwords are handled in Linux, possible ways of  capturing the plain-text password that is entered with sudo, and the motivation behind chosing a rootkit to solve that task. 

%=======================================================================
\section{Problem definition}
%=======================================================================
For security reasons, user account passwords on Linux are never stored with their real, plain-text values on the file system \cite{Nemeth2007}. Instead, they first go through a one-way hashing function, such as Blowfish \cite{Blowfish} or SHA-512 \cite{Sha512}. One of the key properties of these hashing functions is that it is unfeasible to obtain the original, plain-text  password from the hashed version. To increase their security even further, hash functions are designed in a way so that small changes to the input result in completely different hashes \cite{Quynh2012}. Listing \ref{lst:passhash} provides an example of a sample string, before and after a hashing function is applied. The process is repeated once more with an almost identical string that differ in one letter. This tiny difference results in two hashes that do not have even a single character that matches value and position. Passwords are usually stored in their hashed form (Listing \ref{lst:linuxpasswords}), and when a user enters a password, it is hashed using the same algorithm, and then the result is compared with the stored hash. The password hashing techniques aim at increasing the difficulty of passwords leak in the case of a system compromise \cite{Nemeth2007}. 


\begin{lstlisting}[caption={Example operation of SHA-256 hashing function}, label={lst:passhash},showspaces=false,numbers=none]
Input (raw): aaaaaaaaa
Input (base64): YWFhYWFhYWFh
Result (base64): 8qypO4DK5oEiHwRF+k4sroofn4+h4XQdljnKrSIvU30=


Input (raw): aaaaaaaab
Input (base64): YWFhYWFhYWFi
Result (base64): MwwLdJg4W8PnQqJzDm3WU714460HbnRu1WrHKJRyNzE=

\end{lstlisting}



\begin{lstlisting}[label={lst:linuxpasswords},numbers=none,%
caption={Example contents of the \texttt{\textbackslash etc\textbackslash shadow} file which contains the encrypted passwords in standard Linux systems}]
root:$6$4PPXd1K..fY2wVdT$itRp7mvOaDkaNQweWGXF8A4kEv.segEabVOEWzb/ezNFGWQuYdhwH2x0N85YrRnDwMzVqXL4fobgf.5bD6WZV0:17128::::::
rpc:!:16737::::::
rtkit:!:16737::::::
scard:!:16737::::::
user1:$6$0eUeEjo4$hCmlt5Mc7YFNLS0kjMrp5k6XVUDzp.bM/l9HaM/ukPCgX7tRvsm.e1x75IJgm86CXBXe1VBR50aAWnpxwEQEt/:16918:0:99999:7:::

\end{lstlisting}

The superuser password is no exception. The only time when the plain-text password is exposed to the system is before it goes through the hashing function, which occurs either during login or upon executing the sudo command. On many systems, logging in directly or remotely as the root user is not allowed, and instead, administrators use sudo to perform system-wide tasks \cite{Nemeth2007}. Therefore, this thesis concentrates on extracting the password from the sudo process. There are numerous ways how this task could be achieved. An attacker might install a key logger on the system, recording each stroke on the keyboard, and then extract the password from there. Another approach is to patch the sudo executable in a way that allows for the attacker to retrieve the entered password later.

%=======================================================================
\section{LKM Rootkit as a possible solution}
%=======================================================================
The initial approach followed by the author of this thesis, before understanding how the sudo process works, was to locate the place in memory where the entered password is stored. However, a more in-depth analysis of the sudo source code revealed that it is possible to capture the password in an earlier state, as it is being entered by the user. In sudo (Listing \ref{lst:sudosource} contains the relevant portions of the code), the password is retrieved by calling the function \texttt{tgetpass(..)}. It then proceeds with calling the \texttt{getln} function, which in turn calls the built-in system function \texttt{read(fd, \&c, 1)}, where \texttt{fd} is the input file descriptor, \texttt{STDIN} in the usual case, \texttt{\&c} is the buffer in memory where the input will be stored, and the last argument, \texttt{1}, determines the length of the input that will be entered. The function \texttt{read} is a core user-land function that calls the \texttt{sys\_read} system call. This all means that sudo gets the password by reading and processing a single character at a time. Therefore, by successfully intercepting the \texttt{sys\_read} system call it will be possible to extract the plain-text password directly from the sudo process before it is being hashed. 

In order to be able to manipulate the system calls, the solution must work inside kernel space, with kernel capabilities. This can be achieved by implementing our rootkit as a Loadable Kernel Module. This will allow to overwrite the reference to \texttt{sys\_read} inside the syscall table with a reference to a custom handler, and carry out a couple of hiding techniques so that it is harder for the user to detect the presence of the rootkit. 


\begin{lstlisting}[label={lst:sudosource},numbers=none,%
caption={Relevant portions of sudo source code (v. 1.8.21)}]
(conversation.c): 
$$* $ \cdots $ *)
    read_pass:
        /* Read the password unless interrupted. */
        pass = tgetpass(msg->msg, msg->timeout, flags, callback);
        if (pass == NULL)
	        goto err;
        replies[n].reply = strdup(pass);
$$* $ \cdots $ *)

tgetpass.c, function tgetpass(..): 
$$* $ \cdots $ *)
    if (timeout > 0)
        alarm(timeout);
    pass = getln(input, buf, sizeof(buf), ISSET(flags, TGP_MASK));
    alarm(0);
$$* $ \cdots $ *)

tgetpass.c, function getln(..): 
$$* $ \cdots $ *)
    while (--left) {
	nr = read(fd, &c, 1);
	if (nr != 1 || c == '\n' || c == '\r')
	    break;
	if (feedback) {
$$* $ \cdots $ *)
\end{lstlisting}