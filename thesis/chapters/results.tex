%=======================================================================
\chapter{Rootkit demonstration, analysis and future work }
%=======================================================================

After discussing the source code in the previous section, in this section an example of our rootkit running in the real world will be demonstrated and analyzed. Please note that all of the following listings contain real output, without any modifications or adaptions. Using the source code provided in Appendix \ref{ap:rootkit_full} and repeating the described steps will result in the identical output being produced. 

Two virtual machines were used to create the test environment. They are exact copies of one another,  running CentOS Linux 7 \cite{CentOS} with x86\_64 Kernel 3.10.0 without any additional packages installed. The first machine was used for testing the behavior of our rootkit, and the second one was kept intact and served as a benchmark. More tests on different distributions and newer kernels are provided in Section \ref{sec:res:eval}. 

\section{Compiling the module}

Our rootkit is compiled and linked as a normal LKM \cite{Borland2013}. This is accomplished by running \texttt{make} with a target directory pointing to the source files of the current kernel (given by \texttt{\$(uname -r)}) and the directory that contains our rootkit source code (current working directory \texttt{\$(pwd)} in the example). The compiler throws several warnings which are not critical and do not affect the overall flow of the module. The output of the whole process can be seen in Listing \ref{lst:res_compile}. As a result, a complete, fully-functional LKM called \texttt{rootkit.ko} is produced, which can be later directly loaded into the kernel using the \texttt{insmod} command. 

\begin{lstlisting}[label={lst:res_compile},style=bash,%
caption={Compiling rootkit from source code }]
user@infected $ make -C /lib/modules/$(uname -r)/build SUBDIRS=$(pwd)
...
compiler warnings skipped
...
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /home/moni/rootkit/rootkit.mod.o
  LD [M]  /home/moni/rootkit/rootkit.ko
make: Leaving directory `/usr/src/kernels/3.10.0-957.21.3.x86_64'
user@rootkit $
\end{lstlisting}

\section{Demonstration}
\label{sec:res:main}

The following example assumes that the victim user has a simple password \texttt{CORRECTPASSWORD123}, that our rootkit is already compiled with the correct kernel sources, and has the filename \texttt{rootkit.ko}. The output of our rootkit, printed to the kernel ring buffer, is examined using the \texttt{dmesg} command. 

\subsection{Loading the module in kernel}
Once compiled, the module has to be loaded into the kernel by executing the command \texttt{sudo} \texttt{insmod} \texttt{rootkit.ko}. This leads to the execution of \texttt{rootkit\_init} (see Section \ref{sec:results:init}), which in turn hides the module, locates the starting address of the syscall table, and hooks \\ \texttt{sys\_read} and \texttt{sys\_execve}. As part of the output of the kernel ring buffer from our real-world example, provided in Listing \ref{lst:res_load}, the syscall table is successfully discovered at \texttt{0xffffffffafe02940}. The messages that follow serve as an indicator that our rootkit is still running and has not corrupted the kernel while installing the hooks. The last two lines are produced by \texttt{rootkit\_execve} each time sudo is executed, again only as a proof-of-concept that hooking \texttt{sys\_execve} has been successful. At this stage our rootkit is fully initiated, and waits for the victim to run sudo and to enter his password. 

$ \cdots $

\begin{lstlisting}[label={lst:res_load},style=bash,%
caption={Contents of the kernel ring buffer after our rootkit is loaded(only relevant portions are shown)}]
$$* $ \cdots $ *)
[  104.635916] rootkit: module verification failed: signature and/or required key missing - tainting kernel
[  104.636312] rootkit: hi again 
[  104.636313] rootkit: by the way, you can't find me :) 
[  106.476302] rootkit: sys_call_table @ 0xffffffffb5802940
[  106.476306] rootkit: hooking sys_execve
[  106.476314] rootkit: hooking sys_read
[  150.995937] rootkit: running sudo again? sorry, but I'm forced to tamper with it
[  150.995941] rootkit: process name - sudo
$$* $ \cdots $ *)
\end{lstlisting}

\subsection{Capturing credentials}
\label{sec:res:trap}

Going on with the example, let us assume that our rootkit is already loaded and the victim has decided that he wants to read the last two system messages stored in \texttt{/var/log/messages}. This file is accessible only with root privileges, which makes the use of sudo mandatory, and this is exactly the trigger that is needed. Listing \ref{lst:res_victimpov} reveals the point of view of the victim upon trying to execute \texttt{sudo tail -n 2 /var/log/messages}. Despite his confidence in the high complexity of his password, and despite having checked over both shoulders if anyone is looking at his keyboard, he nevertheless types in \texttt{WRONGPASSWORD}, just in case somebody started watching while he was busy looking at the computer screen. He then presses the return key, receives the \emph{Sorry, try again.} error message from sudo, and quickly types in \texttt{CORRECTPASSWORD123}, followed by another return key. Since the latter password is the correct one, the last two lines of \texttt{/var/log/messages} get printed on the screen, as can be seen in the second half of Listing \ref{lst:res_victimpov}. The victim then reads the messages, promptly clears the screen and logouts, firmly believing that his credentials remain secret and secure. 

\begin{lstlisting}[label={lst:res_victimpov},style=bash,%
caption={Point of view of the victim executing sudo and entering one wrong and one correct password with our rootkit loaded}]
user@infected $ sudo tail -n 2 /var/log/messages
[sudo] password for user: 
Sorry, try again.
[sudo] password for user: 
Oct 27 09:48:09 infected sudo:    user : TTY=pts/1 ; PWD=/home/user ; USER=root ; COMMAND=/bin/tail -n 2 /var/log/secure
Oct 27 09:48:09 infected sudo: pam_unix(sudo:session): session opened for user root by user(uid=0)
user@infected $ clear
user@infected $ exit
logout
Connection to 10.10.1.105 closed.
\end{lstlisting}

\begin{lstlisting}[label={lst:res_victimpov_clean},style=bash,%
caption={Point of view of a user executing sudo and entering one wrong and one correct password on a clean system without our rootkit }]
user@clean $ sudo tail -n 2 /var/log/messages
[sudo] password for user: 
Sorry, try again.
[sudo] password for user: 
Oct 27 09:53:28 clean sudo:    user : TTY=pts/1 ; PWD=/home/user ; USER=root ; COMMAND=/bin/tail -n 2 /var/log/secure
Oct 27 09:53:28 clean sudo: pam_unix(sudo:session): session opened for user root by user(uid=0)
user@clean $ clear
user@clean $ exit
logout
Connection to 10.10.1.106 closed.
\end{lstlisting}

\subsection{Employing hiding techniques}

Listing \ref{lst:res_victimpov_clean} contains the output that is printed on the screen after running the same command \texttt{sudo tail -n 2 /var/log/messages} on another machine that has the same configuration, operating system and software as the one that the victim has, but without our rootkit loaded. Naturally, the username, the password, and the user input on the clean machine are the same as on the one of the victim. Comparing it with Listing \ref{lst:res_victimpov} reveals that the behavior, the standard output and the error messages are completely identical on both systems, regardless of the presence of our rootkit. This means that there is absolutely no indication to the user that the normal flow of the kernel has been compromised. 

In addition, thanks to the hiding techniques implemented in our rootkit (see Section \ref{sec:results:init}), it will remain hidden even from standard commands like \texttt{lsmod} or \texttt{rmmod rootkit}, when reading the contents of \texttt{/proc/kallsyms} or \texttt{/proc/modules}, and also when inspecting the \texttt{/sys/module/} directory. Listing \ref{lst:res_hidden} contains the output that is returned after trying any of the aforementioned methods of finding out that our rootkit has been loaded, and after that repeating the same actions, but with for a different, non-existent module, called \texttt{random\_non\_module\_1}. As expected, the commands return ether nothing or an error message stating that the module is not currently loaded, and this is valid for both the non-existent module and the loaded rootkit with hiding techniques. As a further prove that the hiding techniques work, Listing \ref{lst:res_nothidden} contains the output of the same commands, but this time the portion of the source code that deals with the hiding is commented out, thus fully exposing our rootkit. Contrary to  the results found in Listing \ref{lst:ref_hidden}, here every command returns some meaningful output that indicates that our rootkit is already loaded, and \texttt{rmmod rootkit} does not produce any error messages, which means that the module has been successfully unloaded. 

\begin{lstlisting}[label={lst:res_hidden},style=bash,%
caption={Running several commands to prove that our rootkit is truly hidden}]
user@infected $ sudo cat /proc/kallsyms | grep rootkit
user@infected $ sudo cat /proc/modules | grep rootkit
user@infected $ sudo ls -a /sys/module | grep rootkit
user@infected $ sudo lsmod | grep rootkit
user@infected $ sudo rmmod rootkit
rmmod: ERROR: Module rootkit is not currently loaded
user@infected $ 
user@infected $ sudo cat /proc/kallsyms | grep random_non_module_1
user@infected $ sudo cat /proc/modules | grep random_non_module_1
user@infected $ sudo ls -a /sys/module | grep random_non_module_1
user@infected $ sudo lsmod | grep random_non_module_1
user@infected $ sudo rmmod random_non_module_1
rmmod: ERROR: Module random_non_module_1 is not currently loaded
user@infected $ 

\end{lstlisting}

\begin{lstlisting}[label={lst:res_nothidden},style=bash,%
caption={Running the same commands as before, but this time without the hiding techniques in place}]
user@infected2 $ sudo cat /proc/kallsyms | grep rootkit
ffffffffc03e9000 t rootkit_read	[rootkit]
ffffffffc03eb258 b pass_initialized	[rootkit]
ffffffffc03eb260 b pass	[rootkit]
ffffffffc03e9140 t rootkit_execve	[rootkit]
ffffffffc03eb248 b o_execve	[rootkit]
ffffffffc03eb000 d __this_module	[rootkit]
ffffffffc03e9450 t cleanup_module	[rootkit]
ffffffffc03e9320 t init_module	[rootkit]
ffffffffc03eb250 b o_read	[rootkit]
ffffffffc03e9230 t find	[rootkit]
ffffffffc03eb240 b sys_call_table	[rootkit]
ffffffffc03e9450 t rootkit_exit	[rootkit]
ffffffffc03e9320 t rootkit_init	[rootkit]
ffffffffc03e92c0 t rootkit_patch_memory_address	[rootkit]
user@infected2 $ sudo cat /proc/modules | grep rootkit
rootkit 12831 0 - Live 0xffffffffc03e9000 (E)
user@infected2 $ sudo ls -a /sys/module | grep rootkit
rootkit
user@infected2 $ sudo lsmod | grep rootkit
rootkit                12831  0 
user@infected2 $ sudo rmmod rootkit
user@infected2 $ 

\end{lstlisting}

\subsection{Inspecting the captured credentials}
\label{sec:res:harshdmesg}

At last, the most essential part of our rootkit -- capturing the password -- is put to the test. In  Section \ref{sec:res:trap}, the victim, completely unaware of the presence of our rootkit, entered one wrong and one correct password - \texttt{WRONGPASSWORD} and \texttt{CORRECTPASSWORD123}. Listing \ref{lst:res_win} contains the contents of the kernel ring buffer, which is used by our rootkit for output. It can be clearly seen that both inputs have been logged successfully, and the password is in clear text, as it was entered by him.

\begin{lstlisting}[label={lst:res_win},style=bash,numbers=left,%
caption={Contents of the kernel ring buffer after our rootkit is triggered and RETURN key is pressed(only relevant portions are shown)}]
$$* $ \cdots $ *)
[  156.170394] rootkit: and the password is 

WRONGPASSWORD

[  162.039939] rootkit: and the password is 

CORRECTPASSWORD123
$$* $ \cdots $ *)
\end{lstlisting}



%=======================================================================
\section{Evaluation and limitations}
%=======================================================================

In order to evaluate the success of the provided implementation, our rootkit was tested on several different Linux distributions and kernel versions. The following chapter provides analysis of the results of these tests, together with known and discovered limitations. 

\subsection{Analysis}
\label{sec:res:eval}

In order to be considered successful, our rootkit is supposed to achieve a couple of tasks -- obtain the password, entered during the execution of sudo, and remain invisible to the user while doing so. The example provided and discussed throughout Section \ref{sec:res:main} indicates that the implementation that this thesis proposes successfully achieves both tasks. Furthermore, once loaded, it successfully hides itself from the list of currently loaded modules, and cannot be unloaded, thanks to the hiding techniques deployed. 

Our rootkit was tested on several Linux distributions and kernel versions. As it can be observed from the results of the tests, summarized in Table \ref{tbl:eval_tests}, current implementation works flawlessly on kernel versions 4.X and below. On distributions that run newer kernel with version $\geq 5.0$, our rootkit tries to access memory pages that are not mapped by the kernel, which results in \texttt{unable to handle kernel paging request at xxx} error. One of the causes of this error is due to newer kernels having a proper implementation of KASLR \cite{Edge2013} (see Section \ref{sec:fun:syscall}), which causes the kernel image to be loaded in a different location in memory with every system boot and the syscall table search algorithm thus tries to access invalid (not mapped) pages.

\begin{table}[h]
\centering
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{Distribution}                  & \textbf{Kernel version}    & \textbf{Success}    & \textbf{Additional notes}                          \\ \hline
\multirow{2}{*}{Ubuntu Server 19.10}   & \multirow{2}{*}{5.3.0-19}  & \multirow{2}{*}{no} & \tiny{\texttt{unable to handle kernel}}            \\
                                       &                            &                     & \tiny{\texttt{paging request at ffffffffafa00000}} \\ \hline
Ubuntu Server 16.04                    & 4.10.0-38                  & \textbf{yes}        & --                                                 \\ \hline
\multirow{2}{*}{OpenSUSE Tumbleweed}   & \multirow{2}{*}{5.3.7-1}   & \multirow{2}{*}{no} & \tiny{\texttt{unable to handle kernel}}            \\
                                       &                            &                     & \tiny{\texttt{paging request at ffffffff83a00000}} \\ \hline
OpenSUSE 15.1                          & 4.12.14                    & \textbf{yes}        & --                                                 \\ \hline
CentOS 7                               & 3.10.0-957.21.3            & \textbf{yes}        & --                                                 \\ \hline
\multirow{2}{*}{Fedora Workstation 30} & \multirow{2}{*}{5.0.9-301} & \multirow{2}{*}{no} & \tiny{\texttt{unable to handle kernel}}            \\
                                       &                            &                     & \tiny{\texttt{paging request at ffffffff93000000}} \\ \hline
\end{tabular}
\caption{Rootkit tested on multiple Linux distributions and kernel versions}
\label{tbl:eval_tests}
\end{table}



\subsection{Limitations}

Even though the main goal of the thesis has been achieved, there are a few limitations of our approach that were discovered during the implementation and the tests. Some of them are already present in the provided example in Section \ref{sec:res:main}.

\subsubsection{Additional causes for tainting kernel }
While some of the causes of \texttt{tainting kernel} were taken care of by declaring the LKM as licensed under GPL (see Section \ref{sec:results:init}), one crucial one remains unfixed -- our rootkit is not signed by any of the public keys that are stored in the kernel ring of public keys \cite{Torvalds2019}. This results in a warning produced by the kernel while loading the module (see Listing \ref{lst:res_load}). Bypassing this issue is more complicated, and therefore remains outside the scope of this thesis. 

\subsubsection{Wrong location of syscall table}
The method of obtaining the location of the syscall table (see Section \ref{sec:res:loc_syscall}) in memory is a brute-force search of a fixed portion of the memory for the address of \texttt{sys\_close},  which is represented by a sequence of 8 bytes (4 bytes on 32bit systems). The current algorithm assumes that the only place in the kernel memory where this sequence can be found is in the syscall table. It will fail when the same sequence is found, but it is not part of the syscall table, for example if another module for some reason stores the location of \texttt{sys\_close}. 

The current implementation accepts that the kernel is always stored in the same location, and uses that location as a beginning of the search. This location is hardcoded in the code (line 30 in Listing \ref{lst:rootkit_1}). By enabling \emph{KASLR} (see Section \ref{sec:fun:syscall}), the location of the kernel image in memory changes with each system boot, which results in either failure to locate the syscall table, or corrupts the kernel.

\subsubsection{No guarantee for correctness}
Our rootkit captures the password by storing the result of each \texttt{read} syscall that is executed by sudo and has the length of 1. It does not perform any additional checks whether a password is being entered, and, more importantly, whether the captured password is, for example, correct. This is clearly visible in Listing \ref{lst:res_win}, where both passwords that the victim entered are captured as correct, despite the first one being the wrong one. Additionally, our rootkit is triggered only when the current process name is \texttt{sudo}. This means that any program which has this name is considered to be the \emph{real} sudo process and will set off the capturing process, which may result in wrong input being captured. 

%=======================================================================
\section{Future work}
%=======================================================================

There are multiple aspects of the current implementation that could be improved in order to make our rootkit more versatile and efficient. The following section discusses possible future enhancements, derived both from the results and issues described in previous sections, as well as unmentioned ones.  

\subsubsection{Fix KASLR and access to non-existent memory pages}
While testing our rootkit on different Linux distributions, especially on newer kernels, it was revealed that the Kernel Address--Space Layout Randomization breaks its functionality. KASLR \cite{Edge2013} is a feature that causes the kernel image to be loaded in a different location with every system boot. This means that the hardcoded \texttt{START\_CHECK} and \texttt{END\_CHECK} addresses used to search for the syscall table (see Section \ref{sec:res:loc_syscall}) will not always be valid, which results in an inability to find it and might cause corruption of the running kernel. Solving this issue would include a more sophisticated algorithm that searches through the system memory by not accessing memory pages that do not exist. 

\subsubsection{Additional hiding techniques}
The hiding techniques proposed in this thesis successfully hide the module from the list of the already loaded kernel modules. However, there are still other ways that indicate the presence of our rootkit in the system, such as inspection of log files, contents of directories, or the system memory as a whole \cite{Wampler2007}\cite{Siles204}\cite{Levine2004}. This can be improved through hijacking additional system calls that handle what gets displayed on the screen or outputted to files, and what is returned upon trying to list directory entries. Modified versions of these will look for the name of the rootkit or any other relevant sequence of characters, and remove it from their original results. 

\subsubsection{Hijack other programs that involve passwords and use the same or similar principle}
Sudo is not the only program in Linux that is used to enter passwords and gain access. Other programs, such as \texttt{ssh} \cite{Ssh} or \texttt{login} \cite{Login}, also require users to enter their credentials, which makes them targets of interest. Future versions of the rootkit can listen for user input during their executions and capture passwords from them as well.

\subsubsection{Post-processing of the obtained credentials}
Currently, the password that is captured while running sudo is only logged to the kernel ring buffer -- it is neither checked for validity, nor is it stored in a permanent location. The first issue can be solved by checking the return code of the executed sudo, since it returns 0 if the entered password is correct and the command after that is executed successfully, and 1 otherwise. The solution to the second one can include storing the obtained credentials in a permanent location on the file system, and/or sending them to a remote location for further processing.

