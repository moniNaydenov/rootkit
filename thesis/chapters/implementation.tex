%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{LKM rootkit design and implementation}
\label{sec:results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The aim of this thesis is to provide a working implementation that captures administrator password upon entering it with the sudo process. It will be implemented as a Loadable Kernel Module (LKM), and will require administrative privileges in order to install and run it. When executed, our rootkit shall locate the syscall table in memory, then replace the addresses of two syscalls -- execve and read. Initially, hooking execve was needed to monitor which process is being executed at any given time, and wait for the execution of sudo. However, after further investigation, the information for the currently running process is also available from inside the read syscall, which renders the hooking execve obsolete. Despite that, it remains part of this work as it shows an example of a more complicated syscall hook. The replaced version of read syscall checks each time which process has invoked it, and if it is sudo, records the input inside a buffer, and as soon as the return key is pressed, the captured password is logged to the kernel buffer. Additionally, a simple technique shall be implemented that hides our rootkit from the list of running kernel modules.

The solution is implemented and structured as a basic LKM, and will be referred from now on as \emph{rootkit}. A detailed description of the source code is provided in the following sections.

\section{LKM Initialization}
\label{sec:results:init}

Our rootkit code begins by setting two hardcoded addresses which will be used to define the portion of the memory that will be searched for the location of the syscall table. Listing \ref{lst:rootkit_1} contains the relevant portion of the code. The syscall table is located in a random location inside the kernel memory region which for x64 systems begins at \texttt{0xffffffff81000000} and ends at \texttt{0xffffffff9fffffff}, and is directly mapped to the physical memory starting at \texttt{0x0} \cite{Bovet2005}\cite{Torvalds2019}. The start address, denoted by \texttt{START\_CHECK} in Listing \ref{lst:rootkit_1}, corresponds to the beginning of that region, and the end address, denoted by \texttt{END\_CHECK} -- to the end of the virtual memory. The same is valid for x86 systems, with the difference that the begin and end addresses are 8 bytes (32bit) long. 	\texttt{psize} denotes a new type with the same size as the memory pointer, and depends on the system architecture. 

\lstinputlisting[linerange={25-33},label={lst:rootkit_1},firstnumber=25,%
caption={Virtual memory addresses which define the portion of memory where the syscall table will be searched}]{../rootkit.c}

The code then continues with the initialization of the LKM by defining \texttt{init} and \texttt{exit} functions, and sets up the module license (see lines 60--61 in Listing \ref{lst:rootkit_2}). The \texttt{module\_init} macro defines the first function that gets executed when the module is initially loaded (e.g. after executing \texttt{insmod} command), similarly to the \texttt{main()} function in user-space programs. The function set with \texttt{module\_exit} macro is called respectively when the module is unloaded, enabling its graceful exit \cite{Jongmoo2007}. When loading proprietary modules, the kernel marks itself as \emph{tainted}, a technique used to indicate to the user that something happened that could potentially cause problems \cite{KernelDevTaint}. The \texttt{MODULE\_LICENSE(..)} and \texttt{MODULE\_INFO(..)} macros define our rootkit as licensed under \emph{GPL} \cite{GPL}, which prevents a warning \texttt{tainting kernel} being printed in the kernel ring buffer, which could raise the suspicion of the victim.

\lstinputlisting[linerange={60-63},label={lst:rootkit_2},firstnumber=60,%
caption={Setting up LKM }]{../rootkit.c}

The first method that is actually executed is \texttt{rootkit\_init} (see Listing \ref{lst:rootkit_3} for the source code). It begins by implementing two basic, but effective, hiding techniques -- removing its entry from the \texttt{/proc/modules} and \texttt{/sys/module} Virtual File Systems. Both directories contain the currently loaded kernel modules as a list of sub-directories \cite{TheGeekDiary}\cite{TheGeekDiarya}. Since our rootkit is implemented as a LKM, it is part of that list. Deleting the relevant entries from the list (achieved in lines 66--67 in Listing \ref{lst:rootkit_3}) will result in hiding the loaded rootkit from the kernel and the user. This means that commands such as \texttt{modprobe}, \texttt{modinfo}, \texttt{rmmod} will fail in displaying or unloading the module, even though it is already running. Why and how these techniques work is described in detail in Tyler Borland's article ``Modern Linux Rootkits 101'', and will remain outside the scope of this thesis \cite{Borland2013}. 

Our rootkit continues by printing a couple of ``welcoming'' messages to the kernel ring buffer which serve as a debugging information that the module is loaded. In a real-world rootkit, no extra messages or any other extraordinary information should be printed anywhere to avoid user's suspicion. The initialization then continues by locating the syscall table, and then hooking up execve and read system calls, which are discussed in further detail below.

\lstinputlisting[linerange={66-82},label={lst:rootkit_3},firstnumber=66,%
caption={Initial rootkit method}]{../rootkit.c}

\section{Toggling memory page protection}
As a security precaution, some pages of the memory are protected by the kernel from writing once they are populated when the system loads \cite{Seshadri2007}. During the normal operation of the system, the syscall table is not expected to be changed, and the memory page where it is stored is therefore marked as read-only. The protection is globally controlled via the 16th bit of the \texttt{CR0} control register -- when set to 1, the CPU is not allowed to write to pages that are marked as read-only \cite{IntelCorporation2016}. To disable the protection, this bit has to be set to 0. The way this is achieved can be seen in Listing \ref{lst:rootkit_4}. First, in line 98, \texttt{CR0} is copied to \texttt{value}, which then undergoes a bitwise-AND with an integer that has all its bits set to 0 except the 16th -- line 100. If the result of that operation is bigger than 0, then the \texttt{write protect} flag has been enabled and has to be switched off. This is achieved by doing a bitwise-AND between \texttt{value} and an integer that has all its bits set to 1 except the 16th in line 101, and then writing the result back to \texttt{CR0} in line 102. These bitwise calculations ensure that only the 16th bit is set to 0, while all of other bits keep their original values. The reverse logic is applied to enable page protection again, given in Listing \ref{lst:rootkit_5}. The difference is that the 16th bit is tested if it equals 0 instead of 1 in line 111, and if it does, a bitwise-OR in line 112 achieves the same effect, but sets the 16th bit to 1 instead of 0 and keeps the original values of the other bits, thus enabling the \texttt{write protext} flag.

\lstinputlisting[linerange={98-103},label={lst:rootkit_4},firstnumber=98,%
caption={Disabling write protection}]{../rootkit.c}
\lstinputlisting[linerange={109-114},label={lst:rootkit_5},firstnumber=109,%
caption={Enabling write protection}]{../rootkit.c}

\section{Locating syscall table}
\label{sec:res:loc_syscall}

The location of the syscall table is not exported, and there is no direct way of determining it \cite{One1996}. This partially serves as a protection measure \cite{Borland2013}. This means that the system memory has to be thoroughly searched for its location, which can be time-consuming, especially for x64 systems where the virtual memory addresses begin from \texttt{0x0} and end on \texttt{0xffffffffffffffff} -- the last possible memory address. Therefore, the search is limited within the boundaries defined earlier with \texttt{START\_CHECK} and \texttt{END\_CHECK} (Listing \ref{lst:rootkit_1}), which for x64 systems is set to be a little bit less than 2GB of memory and for x86 systems -- 1GB. \texttt{START\_CHECK} points to the virtual address of the beginning of the loaded kernel, predefined in the Linux source code \cite{Torvalds2019}.

Searching for the syscall table works by iterating through each memory address, starting from \texttt{START\_CHECK}, up until either the correct location is found or \texttt{END\_CHECK} address is reached. Since there is no specific indication that the syscall table starts at a certain location x, the code checks whether the value stored at address \texttt{x + \_\_NR\_close} equals the address of the \texttt{sys\_close} system call (line 123 in Listing \ref{lst:rootkit_6}). \texttt{\_\_NR\_close} is equal to the offset of the close system call in the syscall table, and it is defined in the linux source code \cite{Torvalds2019}. In case of a successful match with the \texttt{sys\_close} address, the value stored in x is returned as the beginning of the syscall table. This method of locating the syscall table originates from Borland's article ``Modern Linux Rootkits 101'' \cite{Borland2013}.

\lstinputlisting[linerange={123-138},label={lst:rootkit_6},firstnumber=123,%
caption={Locating the syscall table}]{../rootkit.c}

\section{Hooking read and execve syscalls}

Once having established the location of the syscall table, hooking, i.e. replacing the entry in the table of the original syscall with a modified one, is for a many of the standard syscalls relatively straight-forward. As can be seen in Listing \ref{lst:rootkit_7}, for \texttt{sys\_read} it boils down to replacing the pointers of the currently loaded syscall with the address of custom syscall implemented in our rootkit. This is achieved with the help of the \texttt{xchg} routine which sets the value stored at the memory location pointed by the first parameter to the value of its second parameter, and returns original value (line 215, Listing \ref{lst:rootkit_7}). The original value is then stored in the global \texttt{o\_read} variable. This step is important, because it contains the address of the original \texttt{sys\_read} syscall, and is later used and also during the execution of the modified syscall and when our rootkit is unloaded. Since this operation requires writing to a read-only memory page, page protection has to be disabled before that and then re-enabled again once the syscall is hooked. 

\lstinputlisting[linerange={214-216},label={lst:rootkit_7},firstnumber=214,%
caption={Hooking \texttt{sys\_read}}]{../rootkit.c}

In the early stages of our rootkit development, we considered that hooking the execve call is required in order to monitor which process is being executed, and to trigger the password capturing if sudo was being executed. Later in the process it was noticed that the currently executed user-space process name can be obtained by simply running the \texttt{get\_task\_comm} macro. 

However, for instructional purposes, due to the fact that hooking execve is a more complex task than just replacing pointers, it remained part of that work. The complexity comes from the fact that the address which is stored in the syscall table for execve does not point to the address of  \texttt{sys\_execve} directly, but to the address of a \texttt{stub}, named \texttt{stub\_execve} in the current case. The reason behind that is explained in a comment in the linux source code, saying that ``certain special system calls that need to save a complete full stack frame'' \cite{Torvalds2019}. Inspecting the assembly code for \texttt{stub\_execve} in Listing \ref{lst:stub_execve} reveals that the actual \texttt{sys\_execve} implementation is called at line 8 by executing the \texttt{CALL} procedure \cite{Torvalds2019}. Therefore, in order to hook \texttt{sys\_execve}, the parameter of the \texttt{CALL} procedure has to point to the modified syscall, so that it gets executed instead of the original one.

\begin{lstlisting}[label={lst:stub_execve},language={[x86masm]Assembler},%
caption={\texttt{stub\_execve} source code, found in entry\_64.S in the Linux source code  \cite{Torvalds2019}},%
morekeywords={addq,rsp,movq,r11,rcx,sys\_execve,int\_ret\_from\_sys\_call,rax,ENTRY}
]
ENTRY(stub_execve)
	CFI_STARTPROC
	addq $8, %rsp
	PARTIAL_FRAME 0
	SAVE_REST
	FIXUP_TOP_OF_STACK %r11
	movq %rsp, %rcx
	call sys_execve
	RESTORE_TOP_OF_STACK %r11
	movq %rax,RAX(%rsp)
	RESTORE_REST
	jmp int_ret_from_sys_call
	CFI_ENDPROC
END(stub_execve)
\end{lstlisting}

The solution implemented in our rootkit achieves this task in several steps. First, it searches for \texttt{0xE8} in memory by starting from the beginning of the loaded bytecode of \texttt{stub\_execve}, lines 239-245 in Listing \ref{lst:rootkit_8}. The search is limited to the first 255 bytes, as the total length of the bytecode of the stub is less than that. Not finding \texttt{0xE8} means that the location of syscall table obtained earlier is probably not correct, which is not handled by the current implementation. \texttt{0xE8} is the assembly instruction for the call procedure. It takes a relative address of the next instruction as its only argument, and this value is stored in the 4 bytes following the instruction -- Listing \ref{lst:call_0xe8}. The fact that the address is not absolute means that it cannot be simply replaced with the absolute address of the modified \texttt{sys\_execve} pointed by \texttt{rootkit\_execve}. Thus, the second step is to calculate the correct relative address of \texttt{rootkit\_execve}, put it in the place of the original address, right after the \texttt{0xE8} instruction, and then calculate and store the absolute address of the original function which will be used in \texttt{rootkit\_execve} and upon unloading the module.
 
\begin{lstlisting}[label={lst:call_0xe8},language={[x86masm]Assembler},numbers=none,%
backgroundcolor=\color{white},%
caption={Sample machine code for the call assembly procedure. \texttt{0xE8} represents the instruction, 0x12345678 -- the relative address of the next instruction }]
                    0xE8 0x12 0x34 0x56 0x78
\end{lstlisting}


The relative address of \texttt{rootkit\_execve} is equal to its absolute address (\texttt{new\_addr} in the code) in memory minus the previously discovered absolute address of the 0xE8 instruction (line 241 in Listing \ref{lst:rootkit_8}) minus \texttt{RELATIVE\_CODE\_SIZE} (1 byte for instruction + 4 bytes of the parameter) -- line 249 in Listing \ref{lst:rootkit_8}. This newly obtained address represents the relative offset in memory from the 0xE8 instruction to \texttt{rootkit\_execve}. Next, the absolute address of the original \texttt{sys\_execve} is calculated from the sum of the absolute address of 0xE8, the relative address of \texttt{sys\_execve} (4 bytes following 0xE8), and \texttt{RELATIVE\_CODE\_SIZE} -- line 25 in Listing \ref{lst:rootkit_8}. As mentioned above, this absolute address is stored in a global variable for later use. The last step is to replace the address that comes after 0xE8 with the already calculated relative address of \texttt{rootkit\_execve}, shown in lines 252-253 in Listing \ref{lst:rootkit_8}.

\lstinputlisting[linerange={235-257},label={lst:rootkit_8},firstnumber=235,%
caption={Hooking \texttt{sys\_execve}}]{../rootkit.c}


\section{Modified \texttt{sys\_read} -- obtain the password}

The modified \texttt{sys\_read} call -- \texttt{rootkit\_read} -- is where all the \emph{magic} happens. \texttt{sys\_read} requires three parameters -- \texttt{fd} is the file descriptor from which the input is read which is irrelevant for achieving the task, \texttt{*buf} is the buffer which contains the input itself, and \texttt{count} gives out the size of the input stored in \texttt{*buf}. Inspecting the sudo source code (see relevant portions of the code in Listing \ref{lst:sudosource}) reveals that \texttt{sys\_read} is called separately for each character of the password entered by the user. This is achieved in function \texttt{getln} in Listing \ref{lst:sudosource}. Therefore, it is sufficient to wait for the sudo process to call \texttt{sys\_read} with \texttt{count} parameter equal to 1, and store each character entered that way. The resulting string will contain the entered password. Figure \ref{fig:rootkit_read} contains a visual representation of the algorithm.

\begin{figure}[h]
\centering
\includegraphics[width=4in]{../rootkit_read.png}
\caption{Visual representation of the modified \texttt{sys\_read} }
\label{fig:rootkit_read}
\end{figure}

The first step of the modified syscall is to run the original \texttt{sys\_read} syscall implementation, so that the standard behavior of the system is kept intact. The original \texttt{sys\_read} is already available in the \texttt{o\_read} global variable, captured earlier during the hook process (line 215 in Listing \ref{lst:rootkit_7}), and it is called directly with the parameters with which \texttt{rootkit\_read} is called -- line 143 in Listing \ref{lst:rootkit_9}. As a result, the entered character gets stored in \texttt{buf}. After that the name of the current executable is obtained using the \texttt{get\_task\_comm} macro, and is stored in \texttt{comm}. Then follows a check if it equals ``sudo'', line 148 in Listing \ref{lst:rootkit_9}, and another check if \texttt{count} parameter equals 1 -- line 149. Passing both checks means that sudo is receiving user input, and it contains the actual password. \texttt{pass\_initialized} keeps track of the length of the entered password. It being 0 means that this is the first time the \texttt{sys\_read} is called, and the input of the password has just started, and the buffer which is used to store the captured password, \texttt{pass}, can be cleared out -- line 151 in Listing \ref{lst:rootkit_9}. This step is only executed once at the beginning of the capture. Then the character stored in \texttt{buf} gets appended to the already captured characters in \texttt{pass} -- line 153 in Listing \ref{lst:rootkit_9}. Since sudo is executed in user-space, the content of \texttt{buf} cannot be accessed directly, but by using the \texttt{copy\_from\_user} core kernel function. 

At that point \texttt{pass} contains either a portion-of, or the whole password. Again, inspecting the \texttt{getln} function of sudo source code in Listing \ref{lst:sudosource} reveals that the check whether the password is correct or not happens when the user enters a new-line character (ASCII value of 10). Therefore, the next step is to check whether the last entered character equals 10. This check is combined with ensuring the there is already some input entered and it does not exceed the maximum allowed size for \texttt{pass} which is defined to be 512 -- lines 154-155 in Listing \ref{lst:rootkit_9}. If the checks do not pass, the script assumes that there are still more characters pending, so the value of \texttt{pass\_initialized} is incremented with 1, and the process repeats. If they do pass, however, then capturing the password is considered successfully completed, the content of \texttt{pass} gets printed in the kernel ring buffer, and \texttt{pass\_initialilzed} is reset back to 0, waiting for a new password to be entered -- lines 156-157 in Listing \ref{lst:rootkit_9}. The last step of the process is to clean up the used memory, and to return the same value that was returned from the original \texttt{sys\_read} -- lines 168-169.

\lstinputlisting[linerange={143-168},label={lst:rootkit_9},firstnumber=143,%
caption={rootkit\_read -- capturing the password}]{../rootkit.c}

\section{Unloading the module}

The last part of our rootkit is handling the unload of the module. It is here for the sake of completeness, since the module cannot be unloaded due to being removed from the list of the loaded kernel modules (Listing \ref{lst:rootkit_3}). The unload procedure is implemented in the function \texttt{rootkit\_exit} in Listing \ref{lst:rootkit_10}. In our rootkit, its purpose is to restore the changed addresses in the syscall table to their original values, thus ensuring that the state of the system remains intact. This process is straight-forward, equivalent to the hooking part, but with swapped arguments. For  \texttt{sys\_read}, it boils down to calling the \texttt{xchg} routine once again, this time writing the address of the original syscall in the syscall table -- Listing \ref{lst:rootkit_11}. For \texttt{sys\_execve}, it means to perform the same calculations to obtain the relative address of the original syscall, and write it in the place of the modified one in the syscall table. After these two steps are completed, our rootkit can be safely unloaded -- line 92 in Listing \ref{lst:rootkit_10}. 

\lstinputlisting[linerange={220-222},label={lst:rootkit_11},firstnumber=220,%
caption={Unhooking \texttt{sys\_read}}]{../rootkit.c}

\lstinputlisting[linerange={86-92},label={lst:rootkit_10},firstnumber=86,%
caption={Unloading the module}]{../rootkit.c}
