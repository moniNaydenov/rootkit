%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Fundamentals}
\label{sec:fundamentals}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Linux was created from scratch in 1991 by Linus Torvalds as a free and open-source alternative to UNIX \cite{Brit2018}. Since then it has established itself as the leading operating system (OS) for servers, as it offers a stable and reliable environment, with strict and well defined access management. 

\section{Basic system structure}
Linux has one core component, called the \texttt{kernel}, which controls the system and serves as an interface between the programs that are executed and the underlying hardware. Some of the more notable  tasks that the kernel carries out are process management and multitasking, memory allocation and access, maintaining security and integrity of the system, access control to all kinds of system resources, such as file system and peripherals. It also contains drivers for various hardware, including CPUs, GPUs, storage devices, chipsets, and other devices. \cite{Bovet2005} \cite{Siles204}

At system boot, the image of the kernel is loaded into memory in a special, secure location, called kernel space, and then executed from there. It then runs the \texttt{init} process, which then loads the rest of the OS. All of the processes, including init, are virtually separated from the kernel image \cite{Mauerer2008}. The memory locations in which they reside is called user space. As a design principle, the kernel is not allowed to directly access user space memory, and processes are not allowed to access in any way neither memory of other processes, nor the kernel space memory. Its purpose is to ensure that programs will not interfere other programs or the kernel, which ensures the stability and the security of the system \cite{Mauerer2008}.

\section{System calls and syscall table}
\label{sec:fun:syscall}
Processes from user space are not allowed to access any of the functions or data that belongs to the kernel directly. For this reason, kernel functionality is exposed to the outside through a special kind of functions called \emph{system calls} (syscalls). System calls are used to create and terminate processes, allocate and free system memory, all types of file manipulations like creation, copying, writing, and deleting, hardware and device access and management, and intra- and inter-process communication. \cite{Mauerer2008}\cite{Bovet2005}

For a process to execute a syscall, it has to load up the processor registers and stack memory with the id of the syscall to be executed and the required parameters by that syscall.  Once this is done, the process has to either trigger a special \texttt{0x80} software interrupt or execute the \texttt{syscall} assembly instruction \cite{One1996}. The system then enters kernel mode, where the kernel evaluates the process' request, and when the necessary conditions are met, it proceeds with executing the system call. The ids of the syscalls are predefined in the kernel source code -- see Table \ref{table_syscall} for some examples. The first column of the table denotes the id of the system call, which is loaded in the \emph{eax} register, the second column contains a meaningful name of the function, the third column contains the filename with the relevant code and the rest of the columns contain the types of the parameters that the system call requires. At boot time, the kernel creates a virtual table in memory that contains a mapping between each of these system calls and an address in memory which contains the actual code that is executed. This mapping is called \emph{syscall table} \cite{Bovet2005}.

The syscall table and the actual handlers that are executed when making syscalls are populated only once as the kernel image gets loaded in memory when the system starts up, and they are used from every process being run. This makes them very important for the proper operation of the system, and their modification could easily lead to a crash of the whole system. For example, if the \texttt{close} system call is not working correctly, processes will not be able to close file descriptors successfully. However, this also means that if the code that is executed from a particular system call is modified while maintaining its core functionality, then the modified system call will be executed every time instead of the original one. This modification has to be performed only once, and will remain relevant until the kernel image is reloaded again, which, as noted above, happens only when the system starts. 

As the whole system relies on the proper operation of the syscalls, and the syscall table is the interface to them, it is important that it is not tampered or modified in any way \cite{Siles204}. It is also a common target for rootkits that aim to hijack syscalls \cite{One1996}. In order to make rootkits job more difficult, the kernel hides the exact location of the syscall table, and further employs several strategies, in order to make sure that no process or module is able to manipulate it in any way \cite{One1996}. One of them ensures that once loaded in memory, the corresponding memory pages are marked as read-only. By doing that, when the processor is instructed to write to this area, it will throw a \texttt{``write to a read-only page''} exception, which will result in a \texttt{segmentation fault} and eventual total system blockage \cite{IntelCorporation2016}. It is possible, however, to bypass this protection in a couple of ways. In the same way as marking the memory page ``read-only'', one can change its status again to ``read-write'', without having any noticeable effect on the system \cite{IntelCorporation2016} \cite{PragmaticTHC1999}. This will allow writing to that memory page without causing a \texttt{segmentation fault}. Another way is to disable write-protection globally for the processor. It is controlled from the 16th bit of the \texttt{CR0} control register -- if this bit is set to 0, then write-protection is disabled. This effectively allows the syscall table to be modified, even after the kernel image has been loaded \cite{IntelCorporation2016}. A novel kernel mechanism called \emph{Kernel Address Space Layout Randomization} (KASLR) causes to load the kernel image in a different, random memory location every time the system boots, which raises the difficulty of and finding the location of the syscall table \cite{Edge2013}.

\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{lllllll}
\hline
\multicolumn{1}{|l|}{\textbf{\%eax}} & \multicolumn{1}{l|}{\textbf{Name}} & \multicolumn{1}{l|}{\textbf{Source}}            & \multicolumn{1}{l|}{\textbf{\%ebx}}  & \multicolumn{1}{l|}{\textbf{\%ecx}} & \multicolumn{1}{l|}{\textbf{\%ecx}} & \multicolumn{1}{l|}{\textbf{\%edx}} \\ \hline
\multicolumn{1}{|l|}{1}              & \multicolumn{1}{l|}{sys\_exit}     & \multicolumn{1}{l|}{kernel/exit.c}              & \multicolumn{1}{l|}{int}             & \multicolumn{1}{l|}{-}              & \multicolumn{1}{l|}{-}              & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{2}              & \multicolumn{1}{l|}{sys\_fork}     & \multicolumn{1}{l|}{arch/i386/kernel/process.c} & \multicolumn{1}{l|}{struct.pt\_regs} & \multicolumn{1}{l|}{-}              & \multicolumn{1}{l|}{-}              & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{3}              & \multicolumn{1}{l|}{sys\_read}     & \multicolumn{1}{l|}{fs/read\_write.c}           & \multicolumn{1}{l|}{unsigned int}    & \multicolumn{1}{l|}{char *}         & \multicolumn{1}{l|}{size\_t}        & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{4}              & \multicolumn{1}{l|}{sys\_write}    & \multicolumn{1}{l|}{fs/read\_write.c}           & \multicolumn{1}{l|}{unsigned int}    & \multicolumn{1}{l|}{const char *}   & \multicolumn{1}{l|}{size\_t}        & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{5}              & \multicolumn{1}{l|}{sys\_open}     & \multicolumn{1}{l|}{fs/open.c}                  & \multicolumn{1}{l|}{const char *}    & \multicolumn{1}{l|}{int}            & \multicolumn{1}{l|}{int}            & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{6}              & \multicolumn{1}{l|}{sys\_close}    & \multicolumn{1}{l|}{fs/open.c}                  & \multicolumn{1}{l|}{unsigned int}    & \multicolumn{1}{l|}{-}              & \multicolumn{1}{l|}{-}              & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{7}              & \multicolumn{1}{l|}{sys\_waitpid}  & \multicolumn{1}{l|}{kernel/exit.c}              & \multicolumn{1}{l|}{pid\_t}          & \multicolumn{1}{l|}{unsigned int *} & \multicolumn{1}{l|}{int}            & \multicolumn{1}{l|}{-}              \\ \hline
\multicolumn{1}{|l|}{\vdots}         & \multicolumn{1}{l|}{\vdots}        & \multicolumn{1}{l|}{\vdots}              & \multicolumn{1}{l|}{\vdots}          & \multicolumn{1}{l|}{\vdots}         & \multicolumn{1}{l|}{\vdots}            & \multicolumn{1}{l|}{\vdots}          \\ \hline 

\end{tabular}%
}
\caption{A visual representation of a portion of a sample syscall table for x64 Linux systems. Columns \textbf{\%eax} -- \textbf{\%edx} contain the expected values of the processor registers for the corresponding syscall}
\label{table_syscall}
\end{table}


\section{Linux access management and sudo}
Linux has a powerful, built-in access management. It establishes an agile and safe environment in which users and programs are able to access only the files and directories that are allowed to them \cite{Mauerer2008}. This is achieved in several steps. First, each user is unique to the system, and belongs to one or more groups. Each file in the system has two types of owners -- a user owner and a group owner. It is important to note here that the group that owns it may or may not be one of the groups that the user belongs to. Access to the file is maintained at three levels -- user, group, and all users, and there are three types available -- read, write, and execute. For example, in order for a user to be able to read a certain file, one of these conditions must be met: the user is the owner of the file and it allows read to user the owner, the user belongs to a group and it allows read to the group owner, or the file allows read to all users. Failure to meet these conditions will result in a \texttt{Permission denied} error  \cite{Mauerer2008}. 

There exists, however, a special type of user, called \texttt{superuser}, who has unlimited access to all files and folders in the system \cite{Leahu2014}. It is usually named \texttt{root}, and is used for all kinds of administrative tasks, including managing users and groups, managing drivers and software, and configuring the system \cite{Leahu2014}. Although not necessary, the root user is usually protected by a separate password. Knowing the password usually leads to full access to a system, thus allowing the system to be compromised. 

There are certain ways for a user to run programs as superuser. The most trivial one is to log in as root user, either directly on the system or remotely. However, for security reasons, on many systems logging in directly or remotely using the root user is not allowed. An alternative way is to use the sudo command \cite{Leahu2014}. It takes as an argument the program and its own arguments that has to be executed as root. Upon being run, it asks the user to enter a password. If the password is correct, sudo continues with executing the desired program in the context of the root user.

\section{Loadable Kernel Modules}

One of the ways to extend kernel functionality, for example to install drivers for a new hardware, is through editing the source code and recompiling the kernel image \cite{DeGoyeneche1999}. This technique requires the system to be restarted, for the changes to take effect, which is, in many cases, impractical. Therefore, to mitigate this issue, the kernel allows for extensions to be loaded and unloaded, without interrupting the system. These extensions are called \emph{Loadable Kernel Modules} (LKM) \cite{DeGoyeneche1999} \cite{One1996}. LKMs are structurally similar to standard, user-space programs, as they consist of source code and header files, usually written in C or C\+\+, and are compiled using standard compilers. Listing \ref{lst:lkmsource1} contains an example for an elementary LKM that outputs the message \texttt{Hello, kworld!} when loaded, and \texttt{Bye!} when unloaded. Just like user-space programs, LKMs have a \texttt{main} function, called \texttt{lkm\_init}, which is the first function to be executed when the module is loaded into the kernel. The second function from listing \ref{lst:lkmsource1}, \texttt{lkm\_exit}, is executed when the module is unloaded, and it allows the module to exit gracefully, without corrupting the kernel. LKMs can be dynamically loaded and unloaded with the commands \texttt{modprobe} and \texttt{rmmod} respectively \cite{DeGoyeneche1999} \cite{Borland2013}.

\begin{lstlisting}[label={lst:lkmsource1},%
caption={A very simple Loadable Kernel Module source code}]
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("John Doe");
MODULE_DESCRIPTION("The minimal LKM");
MODULE_VERSION("1.0");

int lkm_init(void) {
	printk("Hello, kworld!");
	return 0;
}

void lkm_exit(void) {
	printk("Bye!");
}

module_init(lkm_init);
module_exit(lkm_exit);
\end{lstlisting}


Each user-space program run in its own, insulated environment. This means it cannot access other programs memory, and is subjected to the limitations of the user with which they are started. Contrarily, LKMs operate in kernel space, running above user-space programs, and thus have access to the whole system \cite{Bovet2005}. They can read and write the memory of other processes, turn kernel features on and off, introduce new or modify existing system calls. Because they operate as virtually part of the kernel, they lack the protection mechanisms that it has that ensure the stable operation of the OS. Errors that occur during execution, such as \texttt{Segmentation fault}, which would typically result in a program failure if they were invoked from a user-space program, could easily corrupt the whole system and make it unusable. On the other hand, it provides LKMs with potentially dangerous abilities like hiding malicious behavior, reading unencrypted sensitive data from other processes, such as private keys or certificates loaded in memory, sniffing network traffic, modifying log files, recording key strokes and mouse movements \cite{Bovet2005} \cite{Siles204} \cite{Borland2013}. The list of possible harmful actions that can be achieved with LKMs is practically infinite, as it is constrained mostly by the limitations of the system itself. Most of the times such harmful behavior is disguised as custom hardware driver or other vital kernel module, which could easily trick users into installing the module and becoming a victim. 

