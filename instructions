s discussed on skype, you will work on the following topic:

- PoC Linux Kernel based (LKM) rootkit with basic hiding functionality and interception of sudo password prompt.

- Motivation: After an attacker gets root access to a vulnerable Linux server in a data center, he might be interested to compromise other servers maintained by the same administrator. The administrator’s password is usually stored only in hashed form on the compromised machine, and if a high-entropy password is being used, bruteforce or dictionary attacks against the password hash are futile. However, it is standard practice for adminstrator to use ‘sudo’ to carry out privileged tasks. In many standard configurations, sudo requires the administrator to enter his password on the host. The goal of this work is estimate how difficult it would be for an attacker to intercept the password entered during the invocation of sudo while staying undetected and stealthy. In the practical part, a proof of concept kernel-mode rootkit shall be designed that hides its presence on the hosts, intercepts the loading of the sudo process to patch the executable code on-the-fly in memory to add password eavesdropping functionality. In the theoretical part, the necessary techniques shall be described, together with all practical issues encountered. In addition, limits, detection methods, and related state-of-the art rootkit techniques shall be discussed, with the exact focus still tbd.

# Initial pointers:

## Primary

* Modern Linux Rootkits 101 http://turbochaos.blogspot.co.at/2013/09/linux-rootkits-101-1-of-3.html

* Linux Device Drivers https://lwn.net/Kernel/LDD3/

* Linux Device Drivers 3 examples updated to work in recent kernels  https://github.com/martinezjavier/ldd3

* Linux Source Code

* Adore Linux Kernel Module Rootkit https://github.com/trimpsyw/adore-ng

* Basics of Making a Rootkit: From syscall to hook! https://d0hnuts.com/2016/12/21/basics-of-making-a-rootkit-from-syscall-to-hook/

## Related:

* vlany, LD_PRELOAD based userland rootkit https://github.com/mempodippy/vlany

* linux rootkit using debug registers https://github.com/falk3n/subversive

* Horsepill linux rootkit https://github.com/r00tkillah/HORSEPILL https://www.blackhat.com/docs/us-16/materials/us-16-Leibowitz-Horse-Pill-A-New-Type-Of-Linux-Rootkit.pdf


# Initial Phase

0. Do own research

1. Write basic LKM rootkit, implementing 1 or 2 hiding techniques, if possible on current kernel version (eg. as used by Ubuntu 16 LTS, current CentOS, etc). 

2. Implement malicious code injection for sudo process. E.g, hook execve syscall, modify .txt section during load to patch with malicious, eavesdropping code.


# Starting vector for academic literature (mainly for written thesis)

- https://www.usenix.org/system/files/conference/woot16/woot16-paper-spisak.pdf

- look at cited work in paper above, use google scholar, etc.
